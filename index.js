const noble = require('noble');
const debug = require('debug')('sdial');
const deviceName = 'Surface Dial';

noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    console.log('scanning');
    noble.startScanning();
  } else {
    noble.stopScanning();
  }
});

noble.on('discover', function(peripheral) {
  //console.log('peripheral', peripheral.advertisement.localName);
  if (peripheral.advertisement.localName === deviceName) {
    noble.stopScanning();
    console.log('peripheral with ID ' + peripheral.id + ' found');

    interrogate(peripheral);
  }
});


function interrogate(peripheral) {

  peripheral.on('disconnect', function() {
    console.log('disconnected');
    process.exit(0);
  });

  peripheral.connect(function(error) {
    console.log('connected');
    peripheral.discoverServices([], function(error, services) {
      services.forEach(function(service) {
        service.discoverCharacteristics([], (error, characteristics) => {
          characteristics.forEach(function(characteristic) {
            console.log(service.name || service.uuid, '|', characteristic.name || characteristic.uuid, characteristic.properties);
            if (characteristic.properties.includes('notify')) {
              //characteristic.on('data', incomingData);
              //characteristic.subscribe();
            }
            if (characteristic.properties.includes('read')) {
              switch (characteristic.name) {
                case 'HID Information':
                  break;
                case 'Report Map':
                  break;
                case 'Peripheral Preferred Connection Parameters':
                  characteristic.read((error, data) => {
                    console.log(characteristic.name, data, String(data));
                  });
                  break;
              }
            }
            if (characteristic.properties.includes('writeWithoutResponse')) {
              switch (characteristic.name) {
                case 'HID Control Point':
                  //characteristic.write(new Buffer('01', 'hex'));
                  break;
              }
            }
          });
        });
      });
    });
  });
}


function incomingData(data, isNotification) {
  console.log('incomingData', isNotification, data);
}
